import logging
import requests
import json
import os
from dotenv import load_dotenv
import re
from typing import Any, Text, Dict, List, Optional
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict
from rasa_sdk.events import AllSlotsReset

from pymongo import MongoClient

logger = logging.getLogger(__name__)
load_dotenv()

client = MongoClient("mongodb://localhost:27017")
db = client["rasa"]
responses = db["responses"]

OPEN_API_URL = "https://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}"
WEATHER_API_KEY = os.environ.get("WEATHER_API_KEY")
MAIL_API_KEY = os.environ.get("MAIL_API_KEY")
MAIL_BASE_URL = os.environ.get("MAIL_BASE_URL")
MAIL_DOMAIN = os.environ.get("MAIL_DOMAIN")
TEST_USER_MAIL = os.environ.get("TEST_USER_MAIL")

regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')


def send_mail(to=None, subject: str = '', text: str = "") -> object:
    if to is None:
        to = [TEST_USER_MAIL]
    MAIL_URL = "{}/{}/messages".format(MAIL_BASE_URL, MAIL_DOMAIN)
    from_obj = "HouseSearch Bot <mailgun@{}>".format(MAIL_DOMAIN)
    return requests.post(
        MAIL_URL,
        auth=("api", MAIL_API_KEY),
        data={"from": from_obj,
              "to": to,
              "subject": subject,
              "text": text})


class ValidateRequestForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_house_form"

    def validate_location(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Location Validation"""
        logger.info(slot_value)
        if slot_value.lower() in ["san fransisco", "chicago", "seattle", "san jose"]:
            response = requests.get(OPEN_API_URL.format(city=slot_value, api_key=WEATHER_API_KEY))
            data = json.loads(response.text)

            dispatcher.utter_message(
                text="{0} Hmm, the weather currently is {1}".format(data['name'], data['weather'][0]['description']))
            return {"location": slot_value}
        else:
            return {"location": None}

    def validate_email(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Email Validation"""
        logger.info(slot_value)
        if re.fullmatch(regex, slot_value):
            return {"email": slot_value}
        else:
            return {"email": None}

    def validate_bedroom(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Bedroom Validation"""
        logger.info(slot_value)
        if slot_value.isnumeric():
            return {"bedroom": slot_value}
        else:
            return {"bedroom": None}

    def validate_bathroom(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Bathroom Validation"""
        logger.info(slot_value)
        if slot_value.isnumeric():
            return {"bathroom": slot_value}
        else:
            return {"bathroom": None}


class SubmitRequestForm(Action):

    def name(self) -> Text:
        return "action_submit_request"

    async def run(self, dispatcher, tracker: Tracker, domain: DomainDict) -> List[Dict[Text, Any]]:
        name = tracker.get_slot('name')
        email = tracker.get_slot('email')
        location = tracker.get_slot('location')
        bedroom = tracker.get_slot('bedroom')
        bathroom = tracker.get_slot('bathroom')

        response = responses.insert_one({
            "name": name,
            "email": email,
            "location": location,
            "bedroom": bedroom,
            "bathroom": bathroom,
        })

        print(send_mail([email],
                        "{} Your House Search Request".format(name),
                        "Details Of House Search Request Below \n"
                        "Location: {} \n"
                        "Bedroom: {}\n"
                        "Bathroom: {}\n".format(location, bedroom, bathroom)))

        dispatcher.utter_message(template="utter_request_details", name="name")
        return [AllSlotsReset()]
